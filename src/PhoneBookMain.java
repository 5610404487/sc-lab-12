import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBookMain {
	public static void main(String[] args){
		String filename = "phonebook.txt";
		ArrayList<PhoneBook> listPhoneBook = new ArrayList<PhoneBook>();
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				String number = data[1].trim();
				PhoneBook book1 = new PhoneBook(name,number);
				listPhoneBook.add(book1);		
			}
			System.out.println("---Java Phone Book---\nName   Phone\n=====  ==========\n");
			for(PhoneBook pb: listPhoneBook){
				System.out.println(pb.toString());
			}
			System.out.println("\n---------------------");
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file.");
		}
	}
}
