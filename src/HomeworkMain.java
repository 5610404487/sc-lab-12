import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class HomeworkMain {
	
	public static void main(String[] args) {
		String filename = "homework.txt";
		ArrayList<Homework> list = new ArrayList<Homework>();
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			double sum = 0;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				Double data1 = Double.parseDouble(data[1].trim());
				Double data2 = Double.parseDouble(data[2].trim());
				Double data3 = Double.parseDouble(data[3].trim());
				Double data4 = Double.parseDouble(data[4].trim());
				Double data5 = Double.parseDouble(data[5].trim());
				sum = (data1+data2+data3+data4+data5)/5.00;
				Homework h = new Homework(name);
				h.setScore(sum);
				list.add(h);
			}
			fileWriter = new FileWriter("average.txt");
			BufferedWriter out = new BufferedWriter(fileWriter);
			
			System.out.println("---Homework Scores--- \n\nName  Average\n====  ====");

			for(Homework l : list){
				System.out.println(l.getName()+"   "+l.getScore());
				out.write(l.getName()+" "+l.getScore());
				out.newLine();
			}
			System.out.println("\n---------------------");
			
			out.flush();
		
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
						fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
	}
	
}

}
