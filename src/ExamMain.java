import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ExamMain {
	public static void main(String[] args) {
		String filename = "Homework.txt";
		ArrayList<Homework> listHomework = new ArrayList<Homework>();
		ArrayList<Homework> listExam = new ArrayList<Homework>();
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				Double s1 = Double.parseDouble(data[1].trim());
				Double s2 = Double.parseDouble(data[2].trim());
				Double s3 = Double.parseDouble(data[3].trim());
				Double s4 = Double.parseDouble(data[4].trim());
				Double s5 = Double.parseDouble(data[5].trim());
				double sum = (s1+s2+s3+s4+s5)/5.00;
				Homework hw = new Homework(name);
				hw.setScore(sum);
				listHomework.add(hw);	
			}
			filename = "exam.txt";
			fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			for (line = buffer.readLine(); line != null; line = buffer.readLine()){
				String[] data = line.split(", ");
				String name = data[0].trim();
				Double s1 = Double.parseDouble(data[1].trim());
				Double s2 = Double.parseDouble(data[2].trim());
				double sum = (s1+s2)/2.00;
				Homework hw = new Homework(name);
				hw.setScore(sum);
				listExam.add(hw);
			}
			System.out.println("---Homework Scores--- \nName    Average\n=====   =====");
			for(Homework h:listHomework){
				System.out.println(h.getName()+"    "+h.getScore());				
			}
			System.out.println("\n---Exam Scores--- \nName    Average\n=====   =====");
			for(Homework e:listExam){
				System.out.println(e.getName()+"    "+e.getScore());				
			}
			System.out.println("\n---------------------");
			
			fileWriter = new FileWriter("average.txt",true);
			BufferedWriter out = new BufferedWriter(fileWriter);

			for(Homework e:listExam){
				out.write(e.getName()+" "+e.getScore());
				out.newLine();
			}
			out.flush();
		
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file.");
		}
		finally {
			try {
				if (fileReader != null)
						fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files.");
			}
		}
	
	}
	
}